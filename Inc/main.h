/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define VCC2_Pin GPIO_PIN_13
#define VCC2_GPIO_Port GPIOC
#define GND0_Pin GPIO_PIN_4
#define GND0_GPIO_Port GPIOA
#define GND1_Pin GPIO_PIN_6
#define GND1_GPIO_Port GPIOA
#define LB_Pin GPIO_PIN_4
#define LB_GPIO_Port GPIOC
#define RB_Pin GPIO_PIN_5
#define RB_GPIO_Port GPIOC
#define L1_Pin GPIO_PIN_6
#define L1_GPIO_Port GPIOC
#define L2_Pin GPIO_PIN_7
#define L2_GPIO_Port GPIOC
#define R1_Pin GPIO_PIN_8
#define R1_GPIO_Port GPIOC
#define R2_Pin GPIO_PIN_9
#define R2_GPIO_Port GPIOC
#define VCC_SIDEA_Pin GPIO_PIN_15
#define VCC_SIDEA_GPIO_Port GPIOA
#define GND_SIDEB_Pin GPIO_PIN_10
#define GND_SIDEB_GPIO_Port GPIOC
#define RF_Pin GPIO_PIN_12
#define RF_GPIO_Port GPIOC
#define SIDE_Pin GPIO_PIN_2
#define SIDE_GPIO_Port GPIOD
#define LF_Pin GPIO_PIN_3
#define LF_GPIO_Port GPIOB
#define VCC_Pin GPIO_PIN_5
#define VCC_GPIO_Port GPIOB
#define GND2_Pin GPIO_PIN_8
#define GND2_GPIO_Port GPIOB
#define GND3_Pin GPIO_PIN_9
#define GND3_GPIO_Port GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
