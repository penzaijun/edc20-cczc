#ifndef _LOGIC_
#define _LOGIC_
#include <stdint.h>
#include "myqueue.h"

extern int time;

extern ACT curact;

typedef struct _NODE
{
  uint8_t next_node_num;
  uint8_t next_road[4];
  uint8_t next_node[4];  
}NODE;

typedef struct _ROAD
{
  uint8_t pre_node;
  uint8_t next_node;
}ROAD;

extern uint8_t PRE_NODE;
extern uint8_t CUR_NODE;
extern uint8_t NEXT_NODE;
void LOGIC_INIT(void);
void DECIDE_NEXT_MOVE(void);
void GAME(void);

#endif

