#ifndef _COMMUNICATION_
#define _COMMUNICATION_
#include<stdint.h>

#define NEXT(a) (a+1)%100

//struct Coordinate;
typedef struct _Coordinate
{
	int x,y;
}Coordinate;

//小车坐标，乘客坐标


extern volatile int carrierA_index,carrierB_index;
extern volatile Coordinate carrierA[100],carrierB[100],passenger_st[5],passenger_ed[5];
extern volatile uint8_t whole_message[72];
extern volatile uint8_t messageReceive[72];


extern volatile uint8_t game_status;//游戏状态 
extern volatile uint8_t passenger_status[5];//乘客状态

extern volatile int score[2];//得分 
extern volatile int foultimes[2];//犯规次数 
extern volatile int passenger_num; //乘客总数 
extern volatile int round_num; //回合数 

void message_process(void);
void message_data(void);


#endif
