#ifndef _MYQUEUE_
#define _MYQUEUE_
#include <stdint.h>
#include "communication.h"

enum STATE {AHEAD,BACK,TURNLEFT,TURNRIGHT,BRAKE,STOP};
#define QUEUE_SIZE 20
#define queue_next(a) (a+1)%QUEUE_SIZE
#define queue_pre(a) (a-1)%QUEUE_SIZE  
#define true 1
#define false 0

typedef struct _Coordinate Coordinate; 

typedef struct _act
{
    Coordinate target;      // target of linear motion 
    float road_angle;       // angle of linear motion
    float angle;            // turn angle at target -180 to 180 (anticlockwise is positiove==leftturn is positive)
}ACT;


//funcion pointer list
typedef char                (*isempty_fptr)  ();
typedef void                (*push_fptr)     (ACT a);
typedef void                (*pushlist_fptr) (ACT* a,int length);
typedef ACT                 (*pop_fptr)      ();
typedef void                (*clear_fptr)    ();



typedef struct _myqueue
{
//data
    uint8_t head;
    uint8_t tail;
    ACT data[QUEUE_SIZE];
//functions
    isempty_fptr  isempty;
    push_fptr     push;
    pushlist_fptr pushlist;
    pop_fptr      pop;
    clear_fptr    clear;
}myqueue;        
extern myqueue STATE_LIST;


void QUEUE_INIT(void);
char isempty(void);
void push(ACT a);
void pushlist(ACT* a,int length);
ACT pop(void);
void clear(void);

#endif

