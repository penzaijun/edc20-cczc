#ifndef _MOTOR_
#define _MOTOR_

void MOTOR_LEFT_AHEAD  (float Vacuum_Rate);
void MOTOR_RIGHT_AHEAD (float Vacuum_Rate);
void MOTOR_LEFT_BACK   (float Vacuum_Rate);
void MOTOR_RIGHT_BACK  (float Vacuum_Rate);
void MOTOR_LEFT_STOP   (void);
void MOTOR_RIGHT_STOP  (void);
int  MOTOR_GET_COMPARE (float Vacuum_Rate);

#endif
