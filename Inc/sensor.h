#ifndef _SENSOR_
#define _SENSOR_
/*
    process sensor signal:
        obtain and save infer_red signal & judge whether the car is running straight    
        obtain mpu6050 signals
*/

//inferred related
#define SENSOR_IR_SIZE 20
#define next(a) (a+1)%SENSOR_IR_SIZE
#define pre(a) (a-1)%SENSOR_IR_SIZE
#define BLACK 1
#define WHITE 0

enum SENSOR_IR_STATES {LINE_NONE,LINE_LEFT,LINE_RIGHT,LINE_BOTH};

extern int SENSOR_IR_LEFT_FRONT [SENSOR_IR_SIZE];
extern int SENSOR_IR_RIGHT_FRONT[SENSOR_IR_SIZE];
extern int SENSOR_IR_LEFT_BACK  [SENSOR_IR_SIZE];
extern int SENSOR_IR_RIGHT_BACK [SENSOR_IR_SIZE];
extern int SENSOR_IR_INDEX;

void SENSOR_IR_DATA_INIT(void);
void SENSOR_IR_DATA_UPDATE(void);
enum SENSOR_IR_STATES SENSOR_IR_STATE(void);
void SENSOR_IR_DATA_SEND(void);
int SENSOR_IR_JUDGE(int* data);

//mpu6050 related
extern volatile float SENSOR_ANGLE_X;
extern volatile float SENSOR_ANGLE_Y;
extern volatile float SENSOR_ANGLE_Z;
extern volatile float SENSOR_ANGLE_AC_X;
extern volatile float SENSOR_ANGLE_AC_Y;
extern volatile float SENSOR_ANGLE_AC_Z;

void SENSOR_MPU_INIT(void);
void SENSOR_MPU_UPDATE(void);
void SENSOR_MPU_SEND(void);

#endif
