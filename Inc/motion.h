#ifndef _MOTION_
#define _MOTION_


struct Coordinate;

enum DIR{FORWARD,BACKWARD};        //car direction
enum TURN_DIR {LEFT,RIGHT,NONE};   //turn direction
extern enum DIR DIRECTION;          

void MOTION_SET_DIRECTION(enum DIR dir);
void MOTION_AHEAD(float left_PWM,float right_PWM);
void MOTION_BACK(float left_PWM,float right_PWM);
void MOTION_STOP(void);
void MOTION_TURN(enum TURN_DIR dir,float inner_PWM,float outer_PWM);


#endif

