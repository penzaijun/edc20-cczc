#include "myqueue.h"
#include "motion.h"

myqueue STATE_LIST;

void QUEUE_INIT()
{
    STATE_LIST.clear=clear;
    STATE_LIST.pop=pop;
    STATE_LIST.push=push;
    STATE_LIST.pushlist=pushlist;
    STATE_LIST.isempty=isempty;
    STATE_LIST.clear();
}

char isempty(void)
{
    if(STATE_LIST.head==STATE_LIST.tail) return true;
    else return false;
}

void push(ACT a)
{
    STATE_LIST.data[STATE_LIST.tail]=a;
    STATE_LIST.tail=queue_next(STATE_LIST.tail);
}

void pushlist(ACT* a,int length)
{
    for(int i=0;i<length;i++)
    {
        STATE_LIST.data[STATE_LIST.tail]=a[i];
        STATE_LIST.tail=queue_next(STATE_LIST.tail);    
    }
}

ACT pop(void)
{
    STATE_LIST.head=queue_next(STATE_LIST.head);
    return STATE_LIST.data[queue_pre(STATE_LIST.head)];
}

void clear(void)
{
    STATE_LIST.head=0;
    STATE_LIST.tail=0;
}
