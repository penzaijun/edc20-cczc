/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"

/* USER CODE BEGIN Includes */
#include "string.h"
#include "sensor.h"
#include "motor.h"
#include "motion.h" 
#include "communication.h"
#include "logic.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
TIM_HandleTypeDef htim1;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
UART_HandleTypeDef huart3;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
enum CAR_STATE {TEST_STOP,TEST_ROAD,TEST_WIFI,TEST_SENSOR,TEST_LINE,TEST_TURN,TEST_GAME,TEST_MPU};
volatile enum CAR_STATE state;
volatile int cnt=0; 
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM1_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_USART1_UART_Init(void);                                    
void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);
                                

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void WifiInit(UART_HandleTypeDef*);
int checkbeginning(void);
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
uint8_t U3Rx_len=1;
uint8_t U2Rx_len=1;
uint8_t U1Rx_len=22;
uint8_t U3Rx[1];
uint8_t U2Rx[1];
uint8_t U1Rx[22];
volatile int message_index=0;

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM1_Init();
  MX_USART2_UART_Init();
  MX_USART3_UART_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */
  HAL_TIM_Base_Start(&htim1);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2);
	HAL_GPIO_WritePin(GPIOA, GND0_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(GPIOA, GND1_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOB, GND2_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(GND3_GPIO_Port,GND3_Pin,GPIO_PIN_RESET);
  HAL_GPIO_WritePin(GND_SIDEB_GPIO_Port,GND_SIDEB_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOC,VCC_Pin,GPIO_PIN_SET);
  HAL_GPIO_WritePin(VCC2_GPIO_Port,VCC2_Pin,GPIO_PIN_SET);
  HAL_GPIO_WritePin(VCC_SIDEA_GPIO_Port,VCC_SIDEA_Pin,GPIO_PIN_SET);
  HAL_UART_Receive_IT(&huart3, (uint8_t *)U3Rx, U3Rx_len);
  HAL_UART_Receive_IT(&huart2, (uint8_t *)U2Rx, U2Rx_len);
 	HAL_UART_Receive_IT(&huart1, (uint8_t *)U1Rx, 1);
	HAL_Delay(100);
  WifiInit(&huart2);
  SENSOR_IR_DATA_INIT();
	SENSOR_MPU_INIT();

  HAL_Delay(1000);
  LOGIC_INIT();
  

 
  
  
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
  switch(state)
  {
    case TEST_STOP:
      MOTION_STOP();
      break;

    case TEST_ROAD:
      SENSOR_IR_DATA_UPDATE();
			SENSOR_IR_DATA_SEND();
      switch(SENSOR_IR_STATE())
      {
        case LINE_BOTH:
          MOTION_STOP();
          break;
        case LINE_LEFT:
          MOTION_TURN(RIGHT,0.25,0.4);
          break;
        case LINE_RIGHT:
          MOTION_TURN(LEFT,0.25,0.4);
          break;
        case LINE_NONE:
          MOTION_AHEAD(0.3,0.3);
          break;
      }
      break;

    case TEST_LINE:
      MOTION_AHEAD(0.3,0.3);
      break;

    case TEST_TURN:
      MOTION_TURN(LEFT,0.25,0.4);
      break;

    case TEST_SENSOR:
      SENSOR_IR_DATA_UPDATE();
			SENSOR_IR_DATA_SEND();
      HAL_Delay(990);
      break;

    case TEST_WIFI:
      message_data();
      char b[20];
			sprintf(b,"cnt=%d \n\n",cnt);
			HAL_UART_Transmit(&huart3,(uint8_t*)b,strlen(b),0xffff);
			HAL_Delay(990);
      break;

    case TEST_GAME:
      GAME();
      time++;
      break;

    case TEST_MPU:
      MOTION_TURN(LEFT,0.25,0.4);
      SENSOR_MPU_SEND();
      HAL_Delay(490);
      break;
    
  }
  HAL_Delay(10);
  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* TIM1 init function */
static void MX_TIM1_Init(void)
{

  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig;

  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 71;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 999;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  HAL_TIM_MspPostInit(&htim1);

}

/* USART1 init function */
static void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART3 init function */
static void MX_USART3_UART_Init(void)
{

  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, VCC2_Pin|L1_Pin|L2_Pin|R1_Pin 
                          |R2_Pin|GND_SIDEB_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GND0_Pin|GND1_Pin|VCC_SIDEA_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, VCC_Pin|GND2_Pin|GND3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : VCC2_Pin L1_Pin L2_Pin R1_Pin 
                           R2_Pin GND_SIDEB_Pin */
  GPIO_InitStruct.Pin = VCC2_Pin|L1_Pin|L2_Pin|R1_Pin 
                          |R2_Pin|GND_SIDEB_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : GND0_Pin GND1_Pin VCC_SIDEA_Pin */
  GPIO_InitStruct.Pin = GND0_Pin|GND1_Pin|VCC_SIDEA_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : LB_Pin RB_Pin RF_Pin */
  GPIO_InitStruct.Pin = LB_Pin|RB_Pin|RF_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : SIDE_Pin */
  GPIO_InitStruct.Pin = SIDE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(SIDE_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LF_Pin */
  GPIO_InitStruct.Pin = LF_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(LF_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : VCC_Pin GND2_Pin GND3_Pin */
  GPIO_InitStruct.Pin = VCC_Pin|GND2_Pin|GND3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	UNUSED(huart);
  //case for bluetooth control
  if(huart->Instance==USART3)
  {
  	switch(U3Rx[0])
    {
      case 's': state=TEST_STOP;break;
      case 'r': state=TEST_ROAD;break;
      case 'l': state=TEST_LINE;break;
      case 'S': state=TEST_SENSOR;break;
      case 'w': state=TEST_WIFI;break;
      case 't': state=TEST_TURN;break;
      case 'g': state=TEST_GAME;break;
      case 'm': state=TEST_MPU;break;
    }
    HAL_UART_Transmit(&huart3,(uint8_t*)U3Rx,1,0xffff);
    HAL_UART_Receive_IT(&huart3, (uint8_t *)U3Rx, U3Rx_len);
  }
  //case for wifi connection
  else if(huart->Instance==USART2)
  {
  	HAL_UART_Transmit(&huart3,(uint8_t*)U2Rx,1,0xffff);
   
		whole_message[message_index]=U2Rx[0];
  	message_index++;
  	if (message_index>=2
        &&whole_message[message_index-2]=='O'
        &&whole_message[message_index-1]=='K')
	  {
  		  cnt++;message_index=0;	
				char b[20];
				sprintf(b,"cnt=%d \n\n",cnt);
				HAL_UART_Transmit(&huart3,(uint8_t*)b,strlen(b),0xffff);
	  } 
  	
	  if (checkbeginning()==1)
	  {
		  message_index=0;
	  } 
	
	  if (message_index>=2
		  && message_index<64 
		  && whole_message[message_index-2]==0x0D
		  && whole_message[message_index-1]==0x0A)
	  {
		  message_index=0;
	  }
  
	  if (message_index>=64)
	  {
		  message_index=0;
		
		  if (whole_message[62]==0x0D
			  && whole_message[63]==0x0A)
			
			
		  {
			  for (int i=0;i<64;++i)
			  {
				  messageReceive[i]=whole_message[i];	
			  } 
			  message_process();
        //message_data();
		  }
	  }
	  HAL_UART_Receive_IT(&huart2,U2Rx,U2Rx_len);
  }  
  //case for mpu9250
  else if(huart->Instance==USART1)
  {
    SENSOR_MPU_UPDATE();    
  }
}

int checkbeginning(void)
{
	if (message_index>=8
		&& whole_message[message_index - 8] == 0x2B
		&& whole_message[message_index - 7] == 0x49
		&& whole_message[message_index - 6] == 0x50
		&& whole_message[message_index - 5] == 0x44
		&& whole_message[message_index - 4] == 0x2C)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void SerialPrintChar(char* output, UART_HandleTypeDef* huart)
{
	unsigned int lenth = strlen(output)+1;
	HAL_UART_Transmit(huart, (uint8_t*)output, lenth, 0xFFFF);
}

//閿熸枻鎷烽敓鑴氫紮鎷?
void WifiInit(UART_HandleTypeDef* huart)
{
  
	SerialPrintChar("AT\r\n", huart);
	//while (cnt!=1);
	HAL_Delay(3000);
	SerialPrintChar("AT+CWMODE=3\r\n", huart);
	//while (cnt!=2);
	HAL_Delay(3000);
	SerialPrintChar("AT+RST\r\n", huart);
	//while (cnt!=3);
	HAL_Delay(3000);
	SerialPrintChar("AT+CWJAP=\"EDC20\",\"12345678\"\r\n", huart);
	//while (cnt!=4);
	HAL_Delay(3000);
	SerialPrintChar("AT+CIPSTART=\"TCP\",\"192.168.1.103\",20000\r\n", huart);
	//while (cnt!=5);
	HAL_Delay(3000);
	SerialPrintChar("AT+CIPSTART=\"TCP\",\"192.168.1.103\",20000\r\n", huart);
	HAL_Delay(3000);
	uint8_t a[]="wifi";
  HAL_UART_Transmit(&huart3,a,5,0xffff);
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
