#include "sensor.h"
#include "main.h"
#include "stm32f1xx_hal.h"
#include "motor.h"
#include "motion.h"
#include "string.h"
#include <stdint.h>

extern UART_HandleTypeDef huart3;
extern UART_HandleTypeDef huart1;

//state of infer-red sensor
int SENSOR_IR_LEFT_FRONT [SENSOR_IR_SIZE];
int SENSOR_IR_RIGHT_FRONT[SENSOR_IR_SIZE];
int SENSOR_IR_LEFT_BACK  [SENSOR_IR_SIZE];
int SENSOR_IR_RIGHT_BACK [SENSOR_IR_SIZE];
int SENSOR_IR_INDEX;


void SENSOR_IR_DATA_INIT()
{
    for(int i=0;i<SENSOR_IR_SIZE;i++)
    {
        SENSOR_IR_LEFT_BACK[i]=0;
        SENSOR_IR_LEFT_FRONT[i]=0;
        SENSOR_IR_RIGHT_BACK[i]=0;
        SENSOR_IR_RIGHT_FRONT[i]=0;
    }
}

void SENSOR_IR_DATA_UPDATE()
{
	SENSOR_IR_INDEX=next(SENSOR_IR_INDEX); 
    SENSOR_IR_LEFT_FRONT[SENSOR_IR_INDEX] =(uint8_t)HAL_GPIO_ReadPin(GPIOB,LF_Pin);
    SENSOR_IR_LEFT_BACK[SENSOR_IR_INDEX]  =(uint8_t)HAL_GPIO_ReadPin(GPIOC,LB_Pin);
    SENSOR_IR_RIGHT_FRONT[SENSOR_IR_INDEX]=(uint8_t)HAL_GPIO_ReadPin(GPIOC,RF_Pin);
    SENSOR_IR_RIGHT_BACK[SENSOR_IR_INDEX] =(uint8_t)HAL_GPIO_ReadPin(GPIOC,RB_Pin);
}

int SENSOR_IR_JUDGE(int* data)
{
    int sum=0;
    int index=SENSOR_IR_INDEX;
    for(int i=0;i<5;i++)
    {
        sum+=data[index];
        index=pre(index);
    }
    if(sum>0) return BLACK;
    else return WHITE;
}

enum SENSOR_IR_STATES SENSOR_IR_STATE()
{
    if(DIRECTION==FORWARD)
    {
        if (SENSOR_IR_JUDGE(SENSOR_IR_LEFT_FRONT)==BLACK && SENSOR_IR_JUDGE(SENSOR_IR_RIGHT_FRONT)==BLACK )
            return LINE_BOTH;
        if (SENSOR_IR_JUDGE(SENSOR_IR_LEFT_FRONT)==BLACK && SENSOR_IR_JUDGE(SENSOR_IR_RIGHT_FRONT)==WHITE )
            return LINE_LEFT;
        if (SENSOR_IR_JUDGE(SENSOR_IR_LEFT_FRONT)==WHITE && SENSOR_IR_JUDGE(SENSOR_IR_RIGHT_FRONT)==BLACK )
            return LINE_RIGHT; 
        if (SENSOR_IR_JUDGE(SENSOR_IR_LEFT_FRONT)==WHITE && SENSOR_IR_JUDGE(SENSOR_IR_RIGHT_FRONT)==WHITE )
            return LINE_NONE;       
    }
    else if (DIRECTION==BACKWARD)
    {
       if (SENSOR_IR_JUDGE(SENSOR_IR_LEFT_FRONT)==BLACK && SENSOR_IR_JUDGE(SENSOR_IR_RIGHT_FRONT)==BLACK )
            return LINE_BOTH;
        if (SENSOR_IR_JUDGE(SENSOR_IR_LEFT_FRONT)==BLACK && SENSOR_IR_JUDGE(SENSOR_IR_RIGHT_FRONT)==WHITE )
            return LINE_RIGHT;
        if (SENSOR_IR_JUDGE(SENSOR_IR_LEFT_FRONT)==WHITE && SENSOR_IR_JUDGE(SENSOR_IR_RIGHT_FRONT)==BLACK )
            return LINE_LEFT; 
        if (SENSOR_IR_JUDGE(SENSOR_IR_LEFT_FRONT)==WHITE && SENSOR_IR_JUDGE(SENSOR_IR_RIGHT_FRONT)==WHITE )
            return LINE_NONE;       
    }
		return LINE_NONE;
}



void SENSOR_IR_DATA_SEND()
{
    char buffer[30];
    sprintf(buffer,"left_front:%d\n",SENSOR_IR_LEFT_FRONT[SENSOR_IR_INDEX]);
    HAL_UART_Transmit(&huart3,(uint8_t*)buffer,strlen(buffer),0xffff);
    sprintf(buffer,"left_back:%d\n",SENSOR_IR_LEFT_BACK[SENSOR_IR_INDEX]);
    HAL_UART_Transmit(&huart3,(uint8_t*)buffer,strlen(buffer),0xffff);
    sprintf(buffer,"right_front:%d\n",SENSOR_IR_RIGHT_FRONT[SENSOR_IR_INDEX]);
    HAL_UART_Transmit(&huart3,(uint8_t*)buffer,strlen(buffer),0xffff);
    sprintf(buffer,"right_back:%d\n",SENSOR_IR_RIGHT_BACK[SENSOR_IR_INDEX]);
    HAL_UART_Transmit(&huart3,(uint8_t*)buffer,strlen(buffer),0xffff);
}


//mpu6050 related
volatile float SENSOR_ANGLE_X;
volatile float SENSOR_ANGLE_Y;
volatile float SENSOR_ANGLE_Z;
volatile float SENSOR_ANGLE_AC_X;
volatile float SENSOR_ANGLE_AC_Y;
volatile float SENSOR_ANGLE_AC_Z;


#define ANGLE_MAX 180      // degree
#define ANGLE_AC_MAX 500   // degree/s

void SENSOR_MPU_INIT(void)
{
    
}

#define NOT_READY 0
#define HEAD1 1
#define HEAD2 2
#define READY 21
 
extern uint8_t U1Rx_len;
extern uint8_t U1Rx[22];
uint8_t mpu_state=NOT_READY;


void SENSOR_MPU_UPDATE(void)
{
    if(mpu_state==READY)
    {
        SENSOR_ANGLE_AC_X=(float)((short)((short)U1Rx[3]<<8|U1Rx[2]))/32768*ANGLE_AC_MAX;
        SENSOR_ANGLE_AC_Y=(float)((short)((short)U1Rx[5]<<8|U1Rx[4]))/32768*ANGLE_AC_MAX;
        SENSOR_ANGLE_AC_Z=(float)((short)((short)U1Rx[7]<<8|U1Rx[6]))/32768*ANGLE_AC_MAX;
        SENSOR_ANGLE_X=(float)((short)((short)U1Rx[14]<<8|U1Rx[13]))/32768*ANGLE_MAX;
        SENSOR_ANGLE_Y=(float)((short)((short)U1Rx[16]<<8|U1Rx[15]))/32768*ANGLE_MAX;
        SENSOR_ANGLE_Z=(float)((short)((short)U1Rx[18]<<8|U1Rx[17]))/32768*ANGLE_MAX;
        //char buffer[20];
        //sprintf(buffer,"MPU:READY %f\n\n",(float)((U1Rx[18]<<8|U1Rx[17])/32768*ANGLE_MAX));
        //HAL_UART_Transmit(&huart3,(uint8_t*)buffer,strlen(buffer),0xffff);
        HAL_UART_Receive_IT(&huart1,U1Rx,U1Rx_len);
    }
    else
    {
        if(mpu_state==NOT_READY && U1Rx[0]==0x55)
        {
            mpu_state++;
            //char buffer[40];
            //sprintf(buffer,"MPU:NOTREADY & RECEIVE RIGHT \n\n");
            //HAL_UART_Transmit(&huart3,(uint8_t*)buffer,strlen(buffer),0xffff);
            HAL_UART_Receive_IT(&huart1,U1Rx,1);
        }
        else if(mpu_state==HEAD1 && U1Rx[0]==0x52)
        {
            mpu_state++;
            //char buffer[20];
            //sprintf(buffer,"MPU:HEAD1 \n\n");
            //HAL_UART_Transmit(&huart3,(uint8_t*)buffer,strlen(buffer),0xffff);
            HAL_UART_Receive_IT(&huart1,U1Rx,1);
        }
        else if(mpu_state>=HEAD2)
        {
            mpu_state++;
            //char buffer[20];
            //sprintf(buffer,"MPU:HEAD2 \n\n");
            //HAL_UART_Transmit(&huart3,(uint8_t*)buffer,strlen(buffer),0xffff);
            HAL_UART_Receive_IT(&huart1,U1Rx,1);
        }
        else 
        {
            mpu_state=NOT_READY;
            HAL_UART_Receive_IT(&huart1,U1Rx,1);
        }
    }
}

void SENSOR_MPU_SEND(void)
{
    char buffer[120];
    sprintf(buffer,"angle_ac:%f \nangle:%f %f %f \n\n",SENSOR_ANGLE_AC_Z,SENSOR_ANGLE_X,SENSOR_ANGLE_Y,SENSOR_ANGLE_Z);
    HAL_UART_Transmit(&huart3,(uint8_t*)buffer,strlen(buffer),0xffff);
}
