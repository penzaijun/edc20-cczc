#include "motor.h"
#include "main.h"
#include "stm32f1xx_hal.h"

extern TIM_HandleTypeDef htim1;

int MOTOR_GET_COMPARE (float Vacuum_Rate)
{
    return (int)(1000*Vacuum_Rate);
}

void MOTOR_LEFT_AHEAD (float Vacuum_Rate)
{
    HAL_GPIO_WritePin(GPIOC, L1_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOC, L2_Pin, GPIO_PIN_SET);
    __HAL_TIM_SET_COMPARE(&htim1,TIM_CHANNEL_1,MOTOR_GET_COMPARE(Vacuum_Rate));
}

void MOTOR_RIGHT_AHEAD (float Vacuum_Rate)
{
    HAL_GPIO_WritePin(GPIOC, R1_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOC, R2_Pin, GPIO_PIN_SET);
    __HAL_TIM_SET_COMPARE(&htim1,TIM_CHANNEL_2,MOTOR_GET_COMPARE(Vacuum_Rate));
}

void MOTOR_LEFT_BACK   (float Vacuum_Rate)
{
    HAL_GPIO_WritePin(GPIOC, L1_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOC, L2_Pin, GPIO_PIN_RESET);
    __HAL_TIM_SET_COMPARE(&htim1,TIM_CHANNEL_1,MOTOR_GET_COMPARE(Vacuum_Rate));
}

void MOTOR_RIGHT_BACK  (float Vacuum_Rate)
{
    HAL_GPIO_WritePin(GPIOC, R1_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOC, R2_Pin, GPIO_PIN_RESET);
    __HAL_TIM_SET_COMPARE(&htim1,TIM_CHANNEL_2,MOTOR_GET_COMPARE(Vacuum_Rate));
}

void MOTOR_LEFT_STOP  ()
{
    HAL_GPIO_WritePin(GPIOC, L1_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOC, L2_Pin, GPIO_PIN_SET);
}

void MOTOR_RIGHT_STOP  ()
{
    HAL_GPIO_WritePin(GPIOC, R1_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOC, R2_Pin, GPIO_PIN_SET);
}
