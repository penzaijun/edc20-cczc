#include "motion.h"
#include "motor.h"

enum DIR DIRECTION=FORWARD;

void MOTION_SET_DIRECTION(enum DIR dir)
{
    DIRECTION=dir;
}

void MOTION_AHEAD(float left_PWM,float right_PWM)
{
    if (DIRECTION==FORWARD)
    {
        MOTOR_LEFT_AHEAD(left_PWM);
        MOTOR_RIGHT_AHEAD(right_PWM);
    }
    else if(DIRECTION==BACKWARD)
    {
        MOTOR_LEFT_BACK(right_PWM);
        MOTOR_RIGHT_BACK(left_PWM);
    }
}

void MOTION_BACK(float left_PWM,float right_PWM)
{
    if (DIRECTION==FORWARD)
    {
        MOTOR_LEFT_BACK(left_PWM);
        MOTOR_RIGHT_BACK(right_PWM);
    }
    else if (DIRECTION==BACKWARD)
    {
        MOTOR_LEFT_AHEAD(right_PWM);
        MOTOR_RIGHT_AHEAD(left_PWM);
    }
}

void MOTION_STOP()
{
    if (DIRECTION==FORWARD)
    {
        MOTOR_LEFT_STOP();
        MOTOR_RIGHT_STOP();
    }
    else if(DIRECTION==BACKWARD)
    {
        MOTOR_LEFT_STOP();
        MOTOR_RIGHT_STOP();
    }
}

void MOTION_TURN(enum TURN_DIR dir,float inner_PWM,float outer_PWM)
{
	if (DIRECTION==FORWARD && dir==LEFT)
    {
        MOTOR_LEFT_BACK(inner_PWM);
        MOTOR_RIGHT_AHEAD(outer_PWM);
    }
    else if (DIRECTION==FORWARD && dir==RIGHT)
    {
        MOTOR_LEFT_AHEAD(outer_PWM);
        MOTOR_RIGHT_BACK(inner_PWM);
    }
    else if (DIRECTION==BACKWARD && dir==LEFT)
    {
        MOTOR_LEFT_BACK(outer_PWM);
        MOTOR_RIGHT_BACK(inner_PWM);
    }
    else if (DIRECTION==BACKWARD && dir==RIGHT)
    {
        MOTOR_LEFT_BACK(inner_PWM);
        MOTOR_RIGHT_BACK(outer_PWM);
    }
}










