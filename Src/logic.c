#include "logic.h"
#include "motion.h"
#include <stdint.h>
#include "myqueue.h"
#include "communication.h"
#include "main.h"
#include "stm32f1xx_hal.h"
#include <string.h>
#include <math.h>
#include "main.h"
#include "sensor.h"

#define pi 3.1415926
extern UART_HandleTypeDef huart3;

int time=0;

#define CARA 1
#define CARB 0

volatile Coordinate* POS_ENEMY;
volatile Coordinate* POS_ME;
volatile int* POS_ENEMY_INDEX;
volatile int* POS_ME_INDEX;
volatile uint8_t side;
//passenger status

uint8_t PASSENGER_ME;
uint8_t PASSENGER_ENEMY; 
uint8_t PASSENGER_NONE=0x00;
uint8_t HAVE_PASSENGER;
uint8_t PASSENGER_ID;
uint8_t PASSENGER_START_AREA;
uint8_t PASSENGER_END_AREA;

uint8_t PASSENGER_START_AREA_LIST[5]; //should be update when receive wifi data
uint8_t PASSENGER_END_AREA_LIST[5];   //should be update when receive wifi data

void PASSENGER_UPDATE()
{
    for(int i=0;i<5;i++)
        if(passenger_status[i]==PASSENGER_ME)
        {
            PASSENGER_ID=i;
            HAVE_PASSENGER=true;
            PASSENGER_START_AREA=PASSENGER_START_AREA_LIST[i];
            PASSENGER_END_AREA=PASSENGER_END_AREA_LIST[i];
            return;
        }
    //no passenger case
    HAVE_PASSENGER=false;    
}


//difinition of area(node)
#define circle    0
#define centre    1
#define crossup   2
#define crossdown 3
#define homeA     4
#define homeB     5

const Coordinate map[]={
    {50,50},        //circle
    {250,250},      //centre
    {100,200},      //crossup
    {200,100},      //crossdown
    {200,50},       //homeA
    {50,200}        //homeB           
};

//definition of area(road)
#define A 6
#define B 7
#define C 8
#define D 9
#define E 10
#define F 11
#define G 12
#define H 13
#define I 14
#define J 15
#define K 16
#define L 17
#define NOWHERE 18

const NODE NODES[6]={
    {3,{D,B,F,255},     {centre,crossup,crossdown,255}          },     //circle
    {4,{H,I,C,L},       {crossup,crossdown,circle,crossdown}    },     //centre
    {3,{G,K,A,255},     {centre,centre,circle,255}              },     //crossup
    {2,{J,E,255,255},   {centre,circle,255,255}                 },     //crossdown
    {2,{E,F,255,255},   {circle,crossdown,255,255}              },     //homea
    {2,{A,B,255,255},   {circle,crossup,255,255}                }      //homeb
};
//has to be ordered by distance by ascending order

const ROAD ROADS[12]={
    {crossup,circle},
    {circle,crossup},
    {centre,circle},
    {circle,centre},
    {crossdown,circle},
    {circle,crossdown},
    {crossup,centre},
    {centre,crossup},
    {centre,crossdown},
    {crossdown,centre},
    {crossup,centre},
    {centre,crossup}
};




uint8_t isPointInRect(int ax,int ay, int bx, int by, int cx, int cy, int dx, int dy,Coordinate e) //判断一点是否在四边形内
																						//abcd为四边形四个顶点坐标，e为乘客坐标
 {
	int aa = (bx - ax)*(e.y - ay) - (by - ay)*(e.x - ax);
	int bb = (cx - bx)*(e.y - by) - (cy - by)*(e.x - bx);
	int cc = (dx - cx)*(e.y - cy) - (dy - cy)*(e.x - cx);
	int dd = (ax - dx)*(e.y - dy) - (ay - dy)*(e.x - dx);
	if ((aa > 0 && bb > 0 && cc > 0 && dd > 0) || (aa < 0 && bb < 0 && cc < 0 && dd < 0))//四边形内的点都在顺时针（逆时针）向量的同一边，即夹角小于90,即向量皆同向
	{
		return true;
	}
	return false;
}

uint8_t GET_AREA(Coordinate c)
{
	if ((c.x - 50)*(c.x - 50) + (c.y - 50)*(c.y - 50) <= 49 * 49)
		return circle;
	else if ((c.x - 162)*(c.x - 162) + (c.y - 163)*(c.y - 163) <= 33 * 33)
		return centre;
	else if (c.x >= 55 && c.x <= 80 && c.y >= 175 && c.y <= 195)
		return homeA;
	else if (c.x >= 175 && c.x <= 195 && c.y >= 55 && c.y <= 80)
		return homeB;
	else if ((c.x - 245)*(c.x - 245) + (c.y - 88)*(c.y - 88) <= 25*25)
		return crossup;
	else if (c.x >= 68 && c.x <= 112 && c.y >= 227 && c.y <= 270)
		return crossdown;
	else if (isPointInRect(91,15,101,32,122,20,117,0,c)|| isPointInRect(117,0,117,20,270,20,270,0,c)|| isPointInRect(250,0,270,0,270,63,250,627,c))//AB智障了，弄麻烦了
		return A;
	else if (isPointInRect(125,20,131,40,102,55,101,32,c)|| isPointInRect(125,20,131,40,235,40,235,20,c)|| isPointInRect(230,40,230,59,250,59,250,40,c)||( (c.x - 235)*(c.x - 235) + (c.y - 35)*(c.y - 35) <= 15*15))//下 中 右 左圆
		return B;
	else if (isPointInRect(99,71,85,85,137,137,151,123,c))
		return C;
	else if (isPointInRect(137,138,123,151,70,99,85,85,c))
		return D;
	else if (isPointInRect (32,101,50,111,40,131,21,122,c)||(c.x >= 20 && c.x <= 40 && c.y >= 122 && c.y <= 235)|| (c.x - 35)*(c.x - 35) + (c.y - 235)*(c.y - 235) <= 15*15|| isPointInRect(40,215,68,227,68,250,40,250,c))//左到右
		return E;
	else if (isPointInRect(32,101,15,91,2,113,20,122,c)|| c.x >= 0 && c.x <=200 && c.y >= 113 && c.y <= 270||((c.x - 35)*(c.x - 35) + (c.y - 235)*(c.y - 235) > 15 * 15&& c.x >= 0 && c.x <= 35 && c.y >= 250 && c.y <= 270)|| c.x >= 35 && c.x <= 68 && c.y >= 25 && c.y <= 27)//左右上
		return F;
	else if (isPointInRect(209,86,172,123,187,137,223,101,c))
		return G;
	else if (isPointInRect(223,101,237,115,200,152,187,137,c))
		return H;
	else if (isPointInRect(123,173,137,187,96,228,82,214,c))
		return I;
	else if (isPointInRect(137,187,151,201,110,242,96,228,c))
		return J;
	else if ((c.x >= 250 && c.x <= 270 && c.y >= 112 && c.y <=187)|| (c.x - 235)*(c.x - 235) + (c.y - 187)*(c.y - 187) <= 35*35|| isPointInRect(200,173,224,197,210,211,186,187,c))//上 圆 下
		return K;
	else if (isPointInRect(186,187,210,211,195,224,173,203,c)|| (c.x - 184)*(c.x - 184) + (c.y - 235)*(c.y - 235) <= 35*35|| (c.x >= 112 && c.x <= 184 && c.y >= 250 && c.y <= 270))
		return L;
	return NOWHERE;
}


uint8_t ROAD_DIS[12]=
{
    10,8,5,5,8,10,5,5,5,5,9,9
};

uint8_t GET_DISTANCE(uint8_t road_index)
{
    return ROAD_DIS[road_index-A];
}


//path
uint8_t ATHOME=true;
uint8_t AREA_LIST[10];
uint8_t AREA_LIST_INDEX;
uint8_t PRE_NODE;
uint8_t CUR_NODE;
uint8_t NEXT_NODE;
void AREA_LIST_CLEAR(void)
{
    AREA_LIST_INDEX=0;
}

const ACT ROAD_ACT[12][4]={
    {{{11,256},180,90},{{10,1185},-90,30},{{24,96},-60,0}}, 
    {{{31,124},120,-30},{{32,236},90,-90},{{59,240},0,0}},
    {{{78,91},-135,0}},
    {{{144,129},45,0}},
    {{{237,32},-90,-90},{{127,30},180,-30},{{106,41},150,0}},
    {{121,9},-30,30},{{258,11},0,90},{{260,68},90,0}},
    {{{130,179},-45,0}},
    {{{108,230},135,0}},
    {{{221,88},-45,0}},
    {{{193,143},135,0}},
    {{{193,258},0,-45},{{208,244},-45,-45},{{208,223},-90,-45},{{180,192},-135,0}},
    {{{226,208},45,-45},{{250,207},0,-60},{{260,185},-60,-30},{{260,120},-90,0}}
};

/*
const uint8_t ROAD_ACT_LEN[]=
{
    3,
};



void GET_INTERROAD_STATE_LIST(uint8_t prenode,uint8_t curnode,uint8_t nextnode)
{

}
*/

float normalize(float in)
{
    if(in>180) in-=360;
    else if(in<-180) in+=360;
    else return in;
}

float offset;

void LOGIC_INIT(void)
{
    QUEUE_INIT();
    if(HAL_GPIO_ReadPin(SIDE_GPIO_Port,SIDE_Pin)==GPIO_PIN_SET)
    {
        side=CARA;
        POS_ME=carrierA;
		POS_ME_INDEX=&carrierA_index;
        POS_ENEMY=carrierB;
		POS_ENEMY_INDEX=&carrierB_index;
        PASSENGER_ME=0x01;
        PASSENGER_ENEMY=0x10;
        offset=normalize( SENSOR_ANGLE_Z-(-90));
    }
    else
    {
        side=CARB;
        POS_ME=carrierB;
		POS_ME_INDEX=&carrierB_index;
        POS_ENEMY=carrierA;
		POS_ENEMY_INDEX=&carrierA_index;
        PASSENGER_ME=0x10;
        PASSENGER_ENEMY=0x01;
        offset=normalize( SENSOR_ANGLE_Z-(-180));
    }

}

//simple math
float dis(Coordinate a,Coordinate b)
{
    return (float)sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
}
float angle(Coordinate origin,Coordinate target)
{
    return (float)atan2(target.y-origin.y,target.x-origin.x)*180/pi;
}

//bfs setting
#define UNDECIDED 255
#define PASSENGER 254
#define DECIDED   0
#define START     253
#define VISTTEDTWICE 252

//logic
void DECIDE_NEXT_MOVE(void)
{
    //ASSERT:STATE_LIST IS EMPTY
    PASSENGER_UPDATE();

    //GAME_STATUS: just started , car is in its home
    //fisrt get to NODE circle
    if(ATHOME==true)
    {
        if(POS_ME_INDEX==&carrierA_index)
        {
            //car A get to circle
            //STATE_LIST.pushlist();
        }
        else
        {
            //STATE_LIST.pushlist();
        }
        ATHOME=false;
        return;
    }

    //GAME_STATUS: just send a passenger to detination and get to next node
    //AREA_LIST is empty
    if(HAVE_PASSENGER==false && AREA_LIST_INDEX==0)
    {
        //decide nearest passenger
        //breadth fisrt search
        //assert cur area is a node
        uint8_t min_dis[18];
        uint8_t pre_area[18][2];//node may be visited twice
        uint8_t area_status[18];
        for(int i=0;i<18;i++)
        {
            min_dis[i]=UNDECIDED; //init value
            pre_area[i][0]=UNDECIDED;
						pre_area[i][1]=UNDECIDED;
					  area_status[i]=UNDECIDED;
        }
        for(int i=0;i<5;i++)
        if(passenger_status[i]==PASSENGER_NONE) area_status[PASSENGER_START_AREA_LIST[i]]=PASSENGER;
        uint8_t temp_node=CUR_NODE;
        uint8_t temp_destination_node=CUR_NODE;        
        //special case: at nearest passenger node
        if(area_status[CUR_NODE]==PASSENGER)
        {
            goto DECIDE_AREA_LIST;
        }
        //normal case
        min_dis[CUR_NODE]=0;pre_area[CUR_NODE][0]=START;area_status[CUR_NODE]=DECIDED;
        
        for(int i=0;i<4;i++)
        {
            uint8_t temp_min_dis=255;
            uint8_t temp_min_dis_index=255;
            for(int j=0;j<NODES[temp_node].next_node_num;j++)
            {
                uint8_t temp_next_node=NODES[temp_node].next_node[j];
                uint8_t temp_next_road=NODES[temp_node].next_road[j];
                uint8_t temp_dis=GET_DISTANCE(temp_next_road);
                if(area_status[temp_next_road]==PASSENGER || area_status[temp_next_node]==PASSENGER)
                //nearest passenger finded 
                {
                    temp_destination_node=temp_next_node;
                    if(pre_area[temp_next_node][0]!=UNDECIDED)
                    {
                        //node has been visited
                        pre_area[temp_next_node][1]=temp_next_road;
                        area_status[temp_next_node]=VISTTEDTWICE;
                    }
                    else
                    {
                        //node hasn't been visited
                        pre_area[temp_next_node][0]=temp_next_road;
                        area_status[temp_next_node]=DECIDED;
                    }
                    pre_area[temp_next_road][0]=temp_node;
                    goto DECIDE_AREA_LIST;
                }
                else
                //not finded nearest passenger
                {
                    if(area_status[temp_next_node]==DECIDED) continue;
                    if(temp_dis<temp_min_dis)
                    {
                        temp_min_dis=temp_dis;
                        temp_min_dis_index=j;
                    }
                    if((min_dis[temp_node]+temp_dis)<min_dis[temp_next_node])
                    {
                        min_dis[temp_next_node]=min_dis[temp_node]+temp_dis;
                        pre_area[temp_next_node][0]=temp_next_road;
                        pre_area[temp_next_road][0]=temp_node;
                    }
                }
            }
            //no new node has been set decided
            if(temp_min_dis_index==255)
            {
                //in this case, only one node can be undecided
                for(int j=0;j<4;j++)
                    if(area_status[j]==UNDECIDED)
                    {
                        area_status[j]=DECIDED;
                        temp_node=j;
                        break;
                    }
            }
            //one new node has been set decided
            else
            {
                uint8_t node_next=NODES[temp_node].next_node[temp_min_dis_index];
                area_status[node_next]=DECIDED;
                temp_node=node_next;
            }
        }
        
        DECIDE_AREA_LIST:
        AREA_LIST_CLEAR();
        //special case: temp_destination_node=CUR_NODE
        if(temp_node==CUR_NODE)
        {

        }
        //normal case
        else
        {
            do
            {
                AREA_LIST[AREA_LIST_INDEX]=temp_destination_node;
                if(area_status[temp_destination_node]==VISTTEDTWICE)
                {
                    temp_destination_node=pre_area[temp_destination_node][1];
                    area_status[temp_destination_node]=DECIDED;
                }
                else
                {
                    temp_destination_node=pre_area[temp_destination_node][0];
                }
                AREA_LIST_INDEX++;
								
            }
            while( !(pre_area[temp_destination_node][0]==START && area_status[temp_destination_node]==DECIDED));
        }
        //AREA_LIST DECIDED  
        //FORMAT: [DESTINATION_NODE ROAD NODE ROAD ... ROAD START_NODE(CUR_NODE)]
    }
    //GAME_STATUS: just get a passenger and get to next node
    //AREA_LIST is empty
    else if(HAVE_PASSENGER==true && AREA_LIST_INDEX==0)
    {
        //decide nearest way to destination
        //breadth fisrt search
        //assert cur area is a node
        uint8_t min_dis[18];
        uint8_t pre_area[18][2];//node may be visited twice
        uint8_t area_status[18];
        for(int i=0;i<18;i++)
        {
            min_dis[i]=UNDECIDED; //init value
            pre_area[i][0]=UNDECIDED;
						pre_area[i][1]=UNDECIDED;
            area_status[i]=UNDECIDED;
        }
        area_status[PASSENGER_END_AREA_LIST[PASSENGER_ID]]=PASSENGER;

        uint8_t temp_node=CUR_NODE;
        uint8_t temp_destination_node=CUR_NODE;        
        //special case: at nearest passenger node
        if(area_status[CUR_NODE]==PASSENGER)
        {
            goto DECIDE_AREA_LIST1;
        }
        //normal case
        min_dis[CUR_NODE]=0;pre_area[CUR_NODE][0]=START;area_status[CUR_NODE]=DECIDED;
        
        for(int i=0;i<4;i++)
        {
            uint8_t temp_min_dis=255;
            uint8_t temp_min_dis_index=255;
            for(int j=0;j<NODES[temp_node].next_node_num;j++)
            {
                uint8_t temp_next_node=NODES[temp_node].next_node[j];
                uint8_t temp_next_road=NODES[temp_node].next_road[j];
                uint8_t temp_dis=GET_DISTANCE(temp_next_road);
                if(area_status[temp_next_road]==PASSENGER || area_status[temp_next_node]==PASSENGER)
                //nearest passenger finded 
                {
                    temp_destination_node=temp_next_node;
                    if(pre_area[temp_next_node][0]!=UNDECIDED)
                    {
                        //node has been visited
                        pre_area[temp_next_node][1]=temp_next_road;
                        area_status[temp_next_node]=VISTTEDTWICE;
                    }
                    else
                    {
                        //node hasn't been visited
                        pre_area[temp_next_node][0]=temp_next_road;
                        area_status[temp_next_node]=DECIDED;
                    }
                    pre_area[temp_next_road][0]=temp_node;
                    goto DECIDE_AREA_LIST1;
                }
                else
                //not finded nearest passenger
                {
                    if(area_status[temp_next_node]==DECIDED) continue;
                    if(temp_dis<temp_min_dis)
                    {
                        temp_min_dis=temp_dis;
                        temp_min_dis_index=j;
                    }
                    if((min_dis[temp_node]+temp_dis)<min_dis[temp_next_node])
                    {
                        min_dis[temp_next_node]=min_dis[temp_node]+temp_dis;
                        pre_area[temp_next_node][0]=temp_next_road;
                        pre_area[temp_next_road][0]=temp_node;
                    }
                }
            }
            //no new node has been set decided
            if(temp_min_dis_index==255)
            {
                //in this case, only one node can be undecided
                for(int j=0;j<4;j++)
                    if(area_status[j]==UNDECIDED)
                    {
                        area_status[j]=DECIDED;
                        temp_node=j;
                        break;
                    }
            }
            //one new node has been set decided
            else
            {
                uint8_t node_next=NODES[temp_node].next_node[temp_min_dis_index];
                area_status[node_next]=DECIDED;
                temp_node=node_next;
            }
        }
        
        DECIDE_AREA_LIST1:
        AREA_LIST_CLEAR();
        //special case: temp_destination_node=CUR_NODE
        if(temp_node==CUR_NODE)
        {

        }
        //normal case
        else
        {
            do
            {
                AREA_LIST[AREA_LIST_INDEX]=temp_destination_node;
                if(area_status[temp_destination_node]==VISTTEDTWICE)
                {
                    temp_destination_node=pre_area[temp_destination_node][1];
                    area_status[temp_destination_node]=DECIDED;
                }
                else
                {
                    temp_destination_node=pre_area[temp_destination_node][0];
                }
                AREA_LIST_INDEX++;
            }
            while( !(pre_area[temp_destination_node][0]==START && area_status[temp_destination_node]==DECIDED));
        }
        //AREA_LIST DECIDED  
        //FORMAT: [DESTINATION_NODE ROAD NODE ROAD ... ROAD START_NODE(CUR_NODE)]
    }


    //GAME_STATUS: AREA_LIST is not empty
    //konwing next area the car should go
    if(AREA_LIST_INDEX>0)
    {
        if(AREA_LIST_INDEX % 2 == 0)
        {
            //since total num of AREA_LIST is odd
            //if the index is even means next STATE_LIST is for a road
            //assert:12>=AREA_LIST[AREA_LIST_INDEX-1]>5 (area is road)
            //STATE_LIST.pushlist();
        }
        else
        {
            //next STATE_LIST is for interroad 
            //STATE_LIST.pushlist();
        }
    } 
}



#define turn 1
#define walk 0

uint8_t popflag=true;
uint8_t walkorturn=walk;
uint8_t FirstFlag=true;
enum TURN_DIR direction;
ACT curact={{0,0},-75,0};
float pwm_left=0.3,pwm_right=0.3;
float pwm_inner=0.2,pwm_out=0.2;
float Pre_Error[100];
float I_Error=0;
float D_Error=0;
uint8_t Pre_Error_index=0;

float init_angle=0;
Coordinate init_target;
uint8_t init_flag=false;

void TEMP_DECIDE_NEXT_MOVE()
{
    if(ATHOME==true && side==CARA)
    {
        //STATE_LIST.pushlist();
        ATHOME=false;
    }
    else if(ATHOME==true && side==CARB)
    {
        //STATE_LIST.pushlist();
        ATHOME=false;
    }
    else
    {
        //STATE_LIST.pushlist();
    }
}





void GAME(void)
{
    if(STATE_LIST.isempty())
        TEMP_DECIDE_NEXT_MOVE();
	if (popflag==true)
	{
		curact=STATE_LIST.pop();
        popflag=false;
	}
	
    float cur_error=normalize(curact.road_angle-SENSOR_ANGLE_Z-offset); // (>0 means right; <0 means left)
	float turn_angle=curact.angle;
	float kp=0.8,ki=0.002,kd=4;
    float pid=(kp*cur_error+ki*I_Error+kd*D_Error)/7200;

    if(walkorturn==walk && (dis(POS_ME[*POS_ME_INDEX],curact.target) <5))
    {
        walkorturn=turn;
        init_angle=SENSOR_ANGLE_Z;
    }
        
    else if(walkorturn==turn && abs(curact.angle - normalize(SENSOR_ANGLE_Z-init_angle))<3)
    {
        walkorturn=walk;
        popflag=true;
    }

    switch(walkorturn)
    { 
		case walk:   //walk
			if(FirstFlag==true)  //The first execution
			{
				FirstFlag=false;
				for(int i=0;i<100;i++)
                {
                    Pre_Error[i]=cur_error;
                }
	    	}
			Pre_Error[Pre_Error_index]=cur_error;
            Pre_Error_index=(Pre_Error_index++)%100;
			I_Error=I_Error+cur_error-Pre_Error[Pre_Error_index];
            float D_Error=cur_error-Pre_Error[(Pre_Error_index+30)%100];
            pwm_left-=pid;
              
            if(pwm_left<0.25) pwm_left=0.25;
		    if(pwm_left>0.4) pwm_left=0.4;
            
            MOTION_AHEAD(pwm_left,pwm_right);
            break;
				
		case turn:   //turn
			if (curact.angle>0)
				MOTION_TURN(LEFT,pwm_inner,pwm_out);
			else 
                MOTION_TURN(RIGHT,pwm_inner,pwm_out);
			break;
	}

    if(time%50==0)
    {
        SENSOR_MPU_SEND();
        char buffer[120];
        sprintf(buffer,"pid:%f"
                       "cur_error:%f"
                       "i_error:%f"
                       "d_error:%f"
                       "walkorturn:%u (walk0,turn1)"
                       "curact_target:(%d,%d)"
                       "mypos:(%d,%d)"
                       "dis_target:%f"
                       "dis_angle"
                       ,pid,cur_error,I_Error,D_Error);
        HAL_UART_Transmit(&huart3,(uint8_t*)buffer,strlen(buffer),0xffff);
    }
}

