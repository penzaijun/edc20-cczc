#include "communication.h"
#include "main.h"
#include "stm32f1xx_hal.h"
#include <stdio.h>
#include <string.h>


//Coordinate carrier[2],passenger_st[4],passenger_ed[4];//小车坐标，乘客坐标 


volatile Coordinate carrierA[100],carrierB[100],passenger_st[5],passenger_ed[5];

volatile int carrierA_index=0,carrierB_index=0;
volatile uint8_t whole_message[72];
volatile uint8_t messageReceive[72];

volatile uint8_t game_status;//游戏状态 
volatile uint8_t passenger_status[5];//乘客状态

extern UART_HandleTypeDef huart3;

volatile int score[2];//得分 
volatile int foultimes[2];//犯规次数 
volatile int passenger_num; //乘客总数 
volatile int round_num; //回合数 

volatile int tmp;
int getnum(int st,int ed,int t){
	tmp=0;int i; 
	for (i=st;i>=ed;--i) tmp+=(messageReceive[t]&(1<<i));
	return tmp>>ed;
}

void message_process(){
	carrierA_index=NEXT(carrierA_index);
	carrierB_index=NEXT(carrierB_index);
	game_status=getnum(7,6,0);
	round_num=getnum(5,0,0)<<8;
	round_num+=getnum(7,0,1);
	
	carrierA[carrierA_index].x=getnum(7,7,2)<<8;
	carrierA[carrierA_index].y=getnum(6,6,2)<<8;
	carrierB[carrierB_index].x=getnum(5,5,2)<<8;
	carrierB[carrierB_index].y=getnum(4,4,2)<<8;
	passenger_st[0].x=getnum(3,3,2)<<8;
	passenger_st[0].y=getnum(2,2,2)<<8;
	passenger_ed[0].x=getnum(1,1,2)<<8;
	passenger_ed[0].y=getnum(0,0,2)<<8;
	
	passenger_st[1].x=(messageReceive[3]&128)<<1;
	passenger_st[1].y=(messageReceive[3]&64)<<2;
	passenger_ed[1].x=(messageReceive[3]&32)<<3;
	passenger_ed[1].y=(messageReceive[3]&16)<<4;
	
	passenger_st[2].x=(messageReceive[3]&8)<<5;
	passenger_st[2].y=(messageReceive[3]&4)<<6;
	passenger_ed[2].x=getnum(1,1,3)<<8;
	passenger_ed[2].y=getnum(0,0,3)<<8;
	
	passenger_st[3].x=getnum(7,7,4)<<8;
	passenger_st[3].y=getnum(6,6,4)<<8;
	passenger_ed[3].x=getnum(5,5,4)<<8;
	passenger_ed[3].y=getnum(4,4,4)<<8;
	
	passenger_st[4].x=getnum(3,3,4)<<8;
	passenger_st[4].y=getnum(2,2,4)<<8;
	passenger_ed[4].x=getnum(1,1,4)<<8;
	passenger_ed[4].y=getnum(0,0,4)<<8;		

	carrierA[carrierA_index].x+=getnum(7,0,5); 
	carrierA[carrierA_index].y+=getnum(7,0,6); 
	carrierB[carrierB_index].x+=getnum(7,0,7); 
	carrierB[carrierB_index].y+=getnum(7,0,8); 
	
	passenger_num=getnum(7,2,9);
	passenger_status[0]=getnum(1,0,9);	
	passenger_status[1]=getnum(7,6,10);
		
	passenger_status[2]=getnum(5,4,10);	
	passenger_status[3]=getnum(3,2,10);	
	passenger_status[4]=getnum(1,0,10);
	
	passenger_st[0].x+=messageReceive[11]; 
	passenger_st[0].y+=messageReceive[12]; 
	passenger_ed[0].x+=messageReceive[13]; 
	passenger_ed[0].y+=messageReceive[14]; 

	passenger_st[1].x+=messageReceive[15]; 
	passenger_st[1].y+=messageReceive[16]; 
	passenger_ed[1].x+=messageReceive[17]; 
	passenger_ed[1].y+=messageReceive[18]; 
	
	passenger_st[2].x+=messageReceive[19]; 
	passenger_st[2].y+=messageReceive[20]; 
	passenger_ed[2].x+=messageReceive[21]; 
	passenger_ed[2].y+=messageReceive[22]; 

	passenger_st[3].x+=messageReceive[23]; 
	passenger_st[3].y+=messageReceive[24]; 
	passenger_ed[3].x+=messageReceive[25]; 
	passenger_ed[3].y+=messageReceive[26]; 

	passenger_st[4].x+=messageReceive[27]; 
	passenger_st[4].y+=messageReceive[28]; 
	passenger_ed[4].x+=messageReceive[29]; 
	passenger_ed[4].y+=messageReceive[30]; 
	
	foultimes[0]=messageReceive[31]; 
	foultimes[1]=messageReceive[32]; 
	
	score[0]=(getnum(7,0,33)<<8)+getnum(7,0,34);
	score[1]=(getnum(7,0,35)<<8)+getnum(7,0,36);	
}

void message_data()
{
	char output[100];
	sprintf(output,"APOS=(%d,%d),BPOS=(%d,%d).\n\n",carrierA[carrierA_index].x,carrierA[carrierA_index].y,carrierB[carrierB_index].x,carrierB[carrierB_index].y);

	HAL_UART_Transmit(&huart3, (uint8_t*)output, strlen(output), 0xFFFF);
} 
